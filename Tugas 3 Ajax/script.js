$(document).ready(function() {
    $("#form-input").submit(function(event) {
        event.preventDefault();

        var nama = $("#nama").val();
        var jenis_kelamin = $('input[name="jenis_kelamin"]:checked').val();
        var kota = $("#kota").val();
        var universitas = $("#universitas").val();

        $.ajax({
            url: "form_process.php",
            type: "post",
            data: {
                nama: nama,
                jenis_kelamin: jenis_kelamin,
                kota: kota,
                universitas: universitas
            },
            success: function(response) {
                $("#output").html(response);
            }
        });
    });
});
